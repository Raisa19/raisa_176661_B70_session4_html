<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Typography</title>
</head>
<body>
    <h1>This is the heading tag</h1>
    <h2>This is the heading tag</h2>
    <h3>This is the heading tag</h3>
    <h4>This is the heading tag</h4>
    <h5>This is the heading tag</h5>
    <h6>This is the heading tag</h6>
    <p>This text is large but<small>This text is small</small></p>
    <p>This is another paragraph.</p>

    <mark>highlight</mark>
    <del>This line of text is meant to be treated as deleted text.</del>
    <u>this text is underlined.</u>
    <b>This text is bold</b>
    <i>This text is italic.</i>
<br>
    <br><blockquote>This is a long quotation.
        This is a long quotation.This is a long quotation.
        This is a long quotation.</blockquote><br>

    <abbr title="World Health Organization">WHO</abbr>

    <acronym title="As Soon As Possible">
        ASAP</acronym>.
<br> <br>

    <address>
        Written by pondit.com<br>
        <a href="mailto:team@pondit.com">Email us</a><br>
        Address: House#16,Road#12,Nikunjo<br>
        Phone: 01675******
    </address>

    <bdo dir="rtl">
        This is LIVEOUTSOURCE.COM
    </bdo>
<br><br>
    <big>This text is big</big>
    <br><br>

    <cite>This Is a Citation</cite>
    <br><br>
    p>Regular text.
    <code>

        <html>
           <p>hello</p>
         <p>world</p>
        </html>
    </code> Regular text.</p>

<xmp>
    <html>
    <p>hello</p>
    <p>world</p>
    </html>

</xmp>


    <dfn>Definition term</dfn>

    <p><dfn id="def-internet">The Internet</dfn> is a global system of interconnected networks that use the Internet Protocol Suite (TCP/IP) to serve billions of users worldwide.</p>



    <h1>Inserted text</h1>
    <p>I am <del>very</del><ins>extremely</ins> happy that you visited this page.</p>
<div>
     hello<u> html</u> world
</div>

    <p>Chemical structure of water is H<sub>2</sub>O.</p>

*
**
***

<pre>*
**
***
</pre>
   <ul style="armenian">
       <li>Bangladesh</li>
       <li>England</li>
       <li>Afganistan</li>
   </ul>

<ol>
    <li></li>
    <li></li>
    <li></li>
    <li></li>

    
</ol>
<table border="1px">
    <tr>

         <th>Name</th>
        <th>Age</th>
        <th>Gender</th>
    </tr>
    <tr>
        <td>Rahim</td>
        <td>33</td>
        <td>male</td>
    </tr>

    <tr>
        <td>Karim</td>
        <td>34</td>
        <td>male</td>
    </tr>

    <tr>
        <td>Fahima</td>
        <td>20</td>
        <td>male</td>
    </tr>

</table>
<table border="1">
<tr>
<th rowspan="2">web language</th>
<td>html</td>
<td>css</td>
</tr>
<tr>
 <td>php</td>
 <td>js</td>

</tr>
<tr>
    <th>framework</th>
<td>ci</td>
 <td>bootstrap</td>
 </tr>
</table>

</body>
</html>